
[Satisficing](https://doc.satisficing.cloud/) website using GitLab Pages.

---

## GitLab CI/CD

This project's static Pages are built by [GitLab CI/CD](https://about.gitlab.com/product/continuous-integration/),

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project.
1. Install Docusaurus:

   ```sh
   cd website
   yarn install
   ```

1. Preview your project:

   ```sh
   yarn start
   ```

   Your site can be accessed under http://localhost:3000.

1. Add content.
1. Generate the website (optional):

   ```sh
   yarn build
   ```

   The website will be built under `website/build/`.

