---
title: Avis
id: microservices-avis
---

[REPOSITORY OFFICIEL](https://gitlab.satisficing.cloud/satisficing/avis)

## Microservice 'Avis' 

Le microservice 'Avis' permet aux utilisateurs de noter et laisser des commentaires via un lien unique générer préalablement

## Prérequis

* REST
* Python
  * Requests
  * Flask
  * PyMongo
  * Pika
* MongoDB

## Ressources utilisées: 

* [Deploy your first Flask+MongoDB app on Kubernetes](https://levelup.gitconnected.com/deploy-your-first-flask-mongodb-app-on-kubernetes-8f5a33fa43b4)
* [Python Flask MongoDB - Complete CRUD in one video](https://www.youtube.com/watch?v=o8jK5enu4L4)


