---
title: Module
id: microservices-module
---

[REPOSITORY OFFICIEL](https://gitlab.satisficing.cloud/satisficing/module)

## Microservice 'Module' 

Ce microservice permet aux formateurs de créer des modules de formation

## Prérequis

* REST
* Python
  * Requests
  * Flask
  * PyMongo
* MongoDB

## Ressources utilisées: 

* [Deploy your first Flask+MongoDB app on Kubernetes](https://levelup.gitconnected.com/deploy-your-first-flask-mongodb-app-on-kubernetes-8f5a33fa43b4)
* [Python Flask MongoDB - Complete CRUD in one video](https://www.youtube.com/watch?v=o8jK5enu4L4)


