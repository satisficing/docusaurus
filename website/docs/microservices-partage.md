---
title: Partage
id: microservices-partage
---

[REPOSITORY OFFICIEL](https://gitlab.satisficing.cloud/satisficing/partage)

## Microservice 'Partage' 

Le microservice 'Partage' permet aux formateurs de généré automatiquement un lien unique lorsqu'un nouveau module de formation est créer. 
La personne qui accède à ce lien est invitée à renseigner son e-mail, ce qui lui permet de participer aux questionnaires de satisfication et aux quizz.

## Prérequis

* REST
* Python
  * Requests
  * Flask
  * PyMongo
* MongoDB

## Ressources utilisées: 

* [Deploy your first Flask+MongoDB app on Kubernetes](https://levelup.gitconnected.com/deploy-your-first-flask-mongodb-app-on-kubernetes-8f5a33fa43b4)
* [Python Flask MongoDB - Complete CRUD in one video](https://www.youtube.com/watch?v=o8jK5enu4L4)



