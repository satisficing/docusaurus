---
title: Présentation 
id: microservices-presentation
slug: /
---

L'application **Satisficing** est composée de **5** microservices, tous responsable d'un sous-domaine spécifique.

* Front
* Formateur
* [Partage](/docs/microservices-partage)
* [Module](/docs/microservices-module)
* [Avis](/docs/microservices-avis)
* Quizz

Chaque microservice dispose d'un pipeline CI/CD et d'une base de données dédiée, elle aussi containerisée.

## Prérequis

* Python
* Docker
* GitlabCI

## Déploiement

Le déploiement et l'orchestration se font via [Kubernetes](/docs/kubernetes-presentation) 

