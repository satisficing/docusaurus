/** @type {import('@docusaurus/types').DocusaurusConfig} */
module.exports = {
  title: 'Satisficing',
  tagline: 'Mesurez aisément la satisfication de vos apprenants',
  url: 'https://satisficing.gitlab.io',
  baseUrl: '/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  organizationName: 'Satisficing', // Usually your GitHub org/user name.
  projectName: 'Satisficing', // Usually your repo name.
  themeConfig: {
    navbar: {
      title: 'Satisficing',
      logo: {
        alt: 'Logo',
        src: 'img/logo.svg',
      },
      items: [
        {
          to: 'docs/',
          activeBasePath: 'docs',
          label: 'Documentation',
          position: 'left',
        },
        {
          label: 'Tarifs',
          position: 'left',
        },
        {
          href: 'https://gitlab.satisficing.cloud/satisficing',
          label: 'Gitlab',
          position: 'right',
        },
        {
          href: 'https://app.satisficing.cloud',
          label: 'Se connecter',
          position: 'right',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Documentation',
          items: [
            {
              label: 'Questions fréquentes',
              to: 'docs/',
            },
          ],
        },
        {
          title: 'Communauté',
          items: [
            {
              label: 'Discord',
              href: 'https://discord.gg/gMm7xuxRan',
            },
          ],
        },
        {
          title: 'Autre',
          items: [
            {
              label: 'Gitlab',
              href: 'https://gitlab.satisficing.cloud',
            },
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} Satisficing`,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          editUrl:
            'https://gitlab.satisficing.cloud/',
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          editUrl:
            'https://gitlab.satisficing.cloud/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
