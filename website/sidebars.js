module.exports = {
  docs: [
    {
      type: 'category',
      label: 'Microservices',
      items: [
        'microservices-presentation',
        'microservices-module',
        'microservices-partage',
        'microservices-avis',
      ],
    },
    {
      type: 'category',
      label: 'Kubernetes',
      items: [
        'kubernetes-presentation',
      ],
    },
  ],
};
